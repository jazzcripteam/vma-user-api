const express = require('express');
const api = express();
const port = 1337;
const mongo = require("mongodb").MongoClient;
const dbuser = "vma-user";
const dbpassword = "fFEQLms8dj5FFW2";
const dbUri = `mongodb://${dbuser}:${dbpassword}@ds263048.mlab.com:63048/vma-api`;

mongo.connect(dbUri, {
   useNewUrlParser: true,
   useUnifiedTopology: true,
   retryWrites: false
}, function (err, client) {
	if (err) return console.log(err);
	console.log(new Date, 'Verify My Age User API DB Connected');
	const dbClient = client.db('vma-api');
	const endpoints = require('./endpoints.js')(api, dbClient);
	api.listen(port, function () {
		console.log(new Date, `Verify My Age User API running in http://localhost:${port}`);
	});
})
