const express = require('express');
const formData = require("express-form-data");
const middleware = fn => (req, res, next) => Promise.resolve(fn(req, res, next)).catch(next);

function isValid(requiredKeys, params) {
	if (typeof params === 'undefined') return false;
	if (requiredKeys.length === 0) return true;
	const keys = Object.keys(params);
	const valid = requiredKeys.some(key => keys.indexOf(key) >= 0 && params[key] !== '');
	return valid;
}

function endpoints(api, dbClient) {
	const controllers = require('./controllers.js')(dbClient);
	const endpointList = {

		createUser: middleware(async function (req, res) {
			const params = req.body;
			const required = ['name', 'age', 'email', 'password', 'address'];
			const valid = isValid(required, params);
			if (valid) {
				let userCreated = await controllers.createUser(params);
				
				if (userCreated) {
					res.sendStatus(201);
				} else {
					res.sendStatus(500);
				}
			} else {
				res.sendStatus(400);
			}
		}),

		updateUser: middleware(async function (req, res) {
			const params = req.body;
			const required = ['email'];
			const valid = isValid(required, params);
			
			if (valid) {
				let userUpdated = await controllers.updateUser(params);
				
				if (userUpdated) {
					res.sendStatus(200);
				} else {
					res.sendStatus(500);
				}
			} else {
				res.sendStatus(400);
			}
		}),

		getUser: middleware(async function (req, res) {
			const params = req.query;
			const required = ['email'];
			const valid = isValid(required, params);
			
			if (valid) {
				let user = await controllers.getUser(params);

				if (user) {
					res.json(user);
				} else {
					res.sendStatus(500);
				}
			} else {
				let users = await controllers.getUsers();

				if (users) {
					res.json(users);
				} else {
					res.sendStatus(500);
				}
			}
		}),

		deleteUser: middleware(async function (req, res) {
			const params = req.body;
			const required = ['email', 'password'];
			const valid = isValid(required, params);
			
			if (valid) {
				let userDeleted = await controllers.deleteUser(params);
				
				if (userDeleted) {
					res.sendStatus(200);
				} else {
					res.sendStatus(500);
				}
			} else {
				res.sendStatus(400);
			}
		})
	};

	api.disable('etag');
	
	api.use(formData.parse({
	  autoClean: true
	}));
	api.use(formData.format());
	api.use(formData.stream());
	api.use(formData.union());
	api.use(express.json());
	api.use(express.urlencoded({extended: true}));

	api.get('/', express.static('public'));

	api.post('/user', endpointList.createUser);

	api.put('/user', endpointList.updateUser);

	api.get('/user/:email?', endpointList.getUser);

	api.post('/user/delete/:email', endpointList.deleteUser);
}

module.exports = endpoints;