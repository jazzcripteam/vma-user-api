# README #

Verify My Age User API.

### Requirements ###

* Create a REST API for a CRUD of users.
* Required fields: `name`, `age`, `email`, `password`, `address`

### Endpoints ###

> `POST` /user
* Create user
Required fields: `name`, `age`, `email`, `password`, `address`

Expected HTTP response: `201 Created`

> `PUT` /user
* Update user info
Required fields: `email`, `password`

Expected HTTP response: `200 OK`

> `GET` /user
* Retrieve a list of users

Expected HTTP response: `200 OK`

> `GET` /user/{`email`}
* Retrieve user info
Required fields: `email`

Expected HTTP response: `200 OK`

> `POST` /user/delete/{`email`}
* Delete user
Required fields: `email`, `password`

Expected HTTP response: `200 OK`

### Basic Stateless API CRUD ###
* Ideally the user password would never travel using only HTTP, HTTPS with a valid certificate would be a production choice. 
* No password field would be required in a more robust authenticated API
* The stored password would have been hashed at least, not only plain text
* The stored password would be removed from any results

### Run Server ###

    npm start

### Access HTML Tester ###
With the API running, you can access a client-side version for test purposes

    http://localhost:1337

### Tests ###

    npm test
