let crypto = require('crypto');
let dbClient;

function isPasswordValid(user, password) {
	return new Promise(async function (resolve, reject) {
		let params = { user	};
		let data = await dbClient.collection('password').findOne(params).catch((err) => false);;

		if (!data) {
			resolve(false);
			return;
		}

		delete data._id;

		let storedHash = data.password;
		let requestedHash = hash({
			user,
			password,
			salt: data.salt
		});

		resolve(storedHash === requestedHash);
	}).catch((err) => false);
}

function hash(params) {
	let keys = Object.keys(params).sort();
	let hashed = crypto.createHash('sha256');
	for (let key of keys) {
		hashed.update(params[key]);
	}
	return hashed.digest('hex').toString();
}

function createUser(params) {
	return new Promise(async function (resolve, reject) {
		let user = params.email;
		let password = params.password;
		let salt = ""+Math.random();
		let hashed = hash({
			user,
			password,
			salt,
		});

		let passwordHash = {
			user,
			password: hashed,
			salt
		};

		delete params.password;

		let opInsertUser = await dbClient.collection('user').insertOne(params).catch((err) => false);
		let opInsertUserOk = opInsertUser && opInsertUser.result.ok === 1;

		if (opInsertUserOk) {
			let opInsertPassword = await dbClient.collection('password').insertOne(passwordHash).catch(() => false);

			let opInsertPasswordOk = opInsertPassword && opInsertPassword.result.ok === 1;
			resolve(opInsertPasswordOk);
		} else {
			resolve(opInsertUserOk);
		}
	});
}

function updateUser(params) {
	return new Promise(async function (resolve, reject) {
		let passwordValid = await isPasswordValid(params.email, params.password);

		if (passwordValid) {
			let updateParams = JSON.parse(JSON.stringify(params));
			let filterParams = ['email', 'password'];

			for (let param of Object.keys(updateParams)) {
				let isFiltered = filterParams.indexOf(param) !== -1;
				let isValued = !!updateParams[param];
				let remove = isFiltered || !isValued;

				if (remove) {
					delete updateParams[param];
				}
			}

			let opUpdate = await dbClient.collection('user').updateOne({ email: params.email }, { $set: updateParams }).catch(() => false);

			let opUpdateOk = opUpdate && opUpdate.result.ok === 1;

			resolve(opUpdateOk);

		} else {
			resolve(false);
		}
	});
}

function getUsers() {
	return new Promise(async function (resolve, reject) {
		let userList = await dbClient.collection('user').find().toArray().catch(() => false);
		
		if (userList) {
			resolve(userList);
		} else {
			resolve(false);
		}
	});
}

function getUser(params) {
	return new Promise(async function (resolve, reject) {
		let data = await dbClient.collection('user').findOne(params).catch(() => false);

		if (data) {
			resolve(data);
		} else {
			resolve(false);
		}
	});
}

function deleteUser(params) {
	return new Promise(async function (resolve, reject) {
		let passwordValid = await isPasswordValid(params.email, params.password);

		if (passwordValid) {
			let opDeleteUser = await dbClient.collection('user').deleteOne({ email: params.email }).catch(() => false);
			let opDeleteUserOk = opDeleteUser && opDeleteUser.result.ok === 1;
			if (opDeleteUserOk) {
				let opDeletePassword = await dbClient.collection('password').deleteOne({ user: params.email }).catch(() => false);
				let opDeletePasswordOk = opDeletePassword && opDeletePassword.result.ok === 1;
				resolve(opDeletePasswordOk);
			} else {
				resolve(false);
			}

		} else {
			resolve(false);
		}
	});
}

function controllers(database) {
	dbClient = database;

	return {
		createUser,
		updateUser,
		getUsers,
		getUser,
		deleteUser
	};
}

module.exports = controllers;